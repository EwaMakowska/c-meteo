﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;

namespace Meteo
{
    class Program
    {
        static void Main(string[] args)
        {

            try
            {
                using (var sr = new StreamReader("..\\..\\..\\daneAll1.json"))
                {
                    List<MeteoDB> items = JsonConvert.DeserializeObject<List<MeteoDB>>(sr.ReadToEnd());

                    items.ForEach(item =>
                    {
                        string requestUri = string.Format("https://maps.googleapis.com/maps/api/geocode/json?key={1}&address={0}&sensor=false", Uri.EscapeDataString(item.Place), "AIzaSyCnuSyhWRRLwqwS5LcCHAuKLlvyqQAPKWQ");

                        WebRequest request = WebRequest.Create(requestUri);
                        WebResponse response = request.GetResponse();
                        using (var rs = response.GetResponseStream())
                        {
                            StreamReader data = new StreamReader(rs, System.Text.Encoding.UTF8);
                            dynamic googleResponse = JsonConvert.DeserializeObject(data.ReadToEnd());

                            item.Location = googleResponse.results[0].geometry.location;
                        }
                    });

                    Console.WriteLine("Wybierz: \n 1 - szukam po nazweie miejsca \n 2 - szukam po długości i szerokości geograficznej");
                    int.TryParse(Console.ReadLine(), out int choise);

                    switch(choise)
                    {
                        case 1:
                            {
                                Console.WriteLine("Podaj miejscowość");
                                string localization = Console.ReadLine();
                                MeteoDB found = items.Find(item => item.Place.Equals(localization));
                                Console.WriteLine(JsonConvert.SerializeObject(found, Formatting.Indented));
                                break;
                            }
                        case 2:
                            {
                                Console.WriteLine("Podaj długosć geograficzną");
                                String lat = Console.ReadLine();
                                Console.WriteLine("Podaj szerokość geograficzną");
                                String lng = Console.ReadLine();
                                MeteoDB found = items.Find(item => (item.Location["lat"].ToString().Equals(lat)) && (item.Location["lng"]).ToString().Equals(lng));
                                Console.WriteLine(JsonConvert.SerializeObject(found, Formatting.Indented));
                                break;
                            }
                        default:
                            Console.WriteLine("Podałęś niewłąściwą wartość. Spróbuj jeszcze raz");
                            break;
                    }
                    
                    double maxTempOut = items.Max(item => Double.Parse(item.TempOut.Replace(',', '.')));
                    double minTempOut = items.Min(item => Double.Parse(item.TempOut.Replace(',', '.')));
                    Console.WriteLine("Max temp: " + maxTempOut);
                    Console.WriteLine("Min temp: " + minTempOut);
                        

                }
            }
            catch (IOException e)
            {
                Console.WriteLine("The file could not be read:");
                Console.WriteLine(e.Message);
                
            }

        }
    }

    class MeteoDB
    {
        public string Place { get; set; }
        //public string Date;
        //public string Time;
        public string TempOut { get; set; }
        public string HiTemp { get; set; }
        public string LowTemp { get; set; }
        public string outHum { get; set; }
        public string DevPt { get; set; }
        public string WindSpeed { get; set; }
        public string WindDir { get; set; }
        public string WindRun { get; set; }
        public string HiSpeed { get; set; }
        public string HDir { get; set; }
        public string WindHill { get; set; }
        public string HeatIndex { get; set; }
        public string THWIndex { get; set; }
        public string Bar { get; set; }
        public string Rain { get; set; }
        public string RainRate { get; set; }
        public string HeatDD { get; set; }
        public string CoolDD { get; set; }
        public string InTemp { get; set; }
        public string InHum { get; set; }
        public string InDev { get; set; }
        public string InHeat { get; set; }
        public string InEMC { get; set; }
        public string InAirDensity { get; set; }
        //public string WindSamp { get; set; }
        public string WindTx { get; set; }
        public string ISSRec { get; set; }
        public string Arc_int { get; set; }
        //public string dr;
        public JToken Location { get; set; } 

    }
}
